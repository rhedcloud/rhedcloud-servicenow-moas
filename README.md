# (RHEDcloud) Emory ServiceNow Message Objects and Services

This repository contains the artifacts needed for the pipeline to generate Java MOA classes for use in OpenEAI Applications and Services that integrate with ServiceNow.

## Pipeline output

The default pipeline runs 2 scripts in parallel

1. Generate and build all moas

	This step creates the packages containing MOA classes for the ServiceNow integration. It is stored in the [Downloads](https://bitbucket.org/rhedcloud/rhedcloud-servicenow-moas/downloads/) section of the repository. Several packages are created but the one that apps and services will need is called ```rhedcloud-servicenow-moa-master-{Build Number}.jar```
	
	The Enterprise Objects (EO) are generated then copied to the resources repo, [rhedcloud-servicenow-moas-resource](https://bitbucket.org/rhedcloud/rhedcloud-servicenow-moas-resource/src/master/). Then, the customized EOs from the ```config``` folder are copied and overlaid onto those that were generated.

2. Gen ServiceNow service

	This step generated the hibernate artfacts required by the 'RDBMS Connector' an OpenEAI/OpenII class that handles persistence for services that require it.
	
	These and many other generated artifacts are kept in the pipelines "artifact" storage as a "tar.gz" that can be downloaded and expanded. The hiberate artifacts can then be found in this location:
	
	```openeai-servicegen-servicenow-service/target/servicenow/rhedcloud-servicenow-service/config```
	
